const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
const alertControlsController = require('../controllers/alert-control');
const devicesController = require('../controllers/devices');
const alertsController = require('../controllers/alerts');
const { params } = require('../middlewares');

// Alerts
router.get('/alerts', alertsController.getAlerts);
router.get('/alerts/:alertId', params.alertId, alertsController.getById);

//Get alerts of a user
router.get('/users/:userId/alerts', params.userId, alertsController.getUserAlerts);


// Alerts Control
router.get('/alertControl', alertControlsController.getAlertControls);
router.get('/alertControl/:alertControlId', params.alertControlId, alertControlsController.getAlertControl);
router.put('/alertControl/:alertControlId', params.alertControlId, alertControlsController.updateAlertControl);
router.post('/alertControl', alertControlsController.createAlertControl);
router.get('/alertControlHistory', alertControlsController.getAlertControlHistory);

// Get Alert Control from a specific user
router.get('/alertControlByUser/:userId', params.userId, alertControlsController.getAlertControlsByUserId);

// Check AlertControl
router.post('/checkAlertControl', alertControlsController.checkAlertControl);
router.post('/sendAlertToDoctor', alertControlsController.sendAlertToDoctor);


// Device
router.get('/devices', devicesController.getDevices);
router.get('/devices/:deviceId', params.deviceId, devicesController.getDevice);
router.put('/devices/:deviceId', params.deviceId, devicesController.updateDevice);
router.post('/devices', devicesController.createDevice);
router.delete('/devices/:deviceId', params.deviceId, devicesController.deleteDevice);

// Users
router.get('/users', usersController.getUsers);
router.get('/users/:userId', params.userId, usersController.getUser);
router.put('/users/:userId', params.userId, usersController.updateUser);
router.post('/users', usersController.createUser);
router.delete('/users/:userId', params.userId, usersController.deleteUser);

router.post('/linkedUsers', usersController.getUsersByUserIds);

//Get Specific Role
router.get('/users/:role', params.role, usersController.findAllByRole);

// Login
router.post('/login', usersController.authenticate);

// GET /logout
router.get('/logout', function(req, res, next) {
    if (req.session) {
      // delete session object
      req.session = null;
    }
    res.json();
});

module.exports = router;
