const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;
const modelExists = mongoose.modelNames().includes('User');

const userSchema = new Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    name: String,
    lastName: String,
    phone: String,
    username: { 
        type: String, 
        unique: true,
        required: true,
        trim: true 
    },
    password: { type: String },
    role: { type: String, required: true},
    gender: { type: String },
    birthday: { type: Date },
    address: { type: String },
    linkedUsers: [{
        type: String,
        ref: 'User'
    }]
},
{
    timestamps: true
})
userSchema.pre('save', function (next) {
    var user = this;
    next();
    bcrypt.hash(user.password, 10, function (err, hash){
        if (err) {
        return next(err);
        }
        user.password = hash;
        next();
    })
});
userSchema.pre('update', function (next) {
    var user = this.getUpdate().$set;;
    next();
    bcrypt.hash(user.password, 10, function (err, hash){
        if (err) {
        return next(err);
        }
        user.password = hash;
        next();
    })
});
//authenticate input against database
userSchema.statics.authenticate = function (email, password, callback) {
    User.findOne({ username: email })
    .exec(function (err, user) {
        if (err) {
            return callback(err)
        } else if (!user) {
            var err = new Error('User not found.');
            err.status = 401;
            return callback(err);
        }
        bcrypt.compare(password, user.password, function (err, result) {
            if (result === true) {
            return callback(null, user);
            } else {
            return callback();
            }
        })
    });
}

var User = mongoose.model('User', userSchema);
module.exports = User;
