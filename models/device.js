const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const modelExists = mongoose.modelNames().includes('Device');
let deviceModel;

if(modelExists){
    deviceModel = mongoose.model('Device');
} else{
    const deviceSchema = new Schema({
        _id: {
            type: String,
            required: true,
            unique: true,
            lowercase: true
        },
        user: {
            type: String,
            ref: 'User'
        },
        description: String,
        assignmentDate: Date,
    }, {
        timestamps: true
    });

    deviceSchema.virtual('uuid')
        .get(function getUuid() { return this._id })
        .set(function setUuid(value) { this._id = value });

    deviceModel = mongoose.model('Device', deviceSchema);
}

module.exports = deviceModel;
