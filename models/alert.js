const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const modelExists = mongoose.modelNames().includes('Alert');

const alertSchema = new Schema({
    user: {
        type: String,
        ref: 'User'
    },
    device: {
        type: String,
        ref: 'Device'
    },
    type: Number,
    observations: String,
    diagnosis: String,
    seenBy: [{
        type: String,
        ref: 'User'
    }],
    details: {
        ecg: [Number],
        longitud: Number,
        latitud: Number,
        spo2: String,
        ps: Number,
        pa: Number,
        fc: Number,
        rr: Number,
        pr: Number,
        qrs: Number,
        st: Number,
        qt: Number,
        verde: Number,
        arritmia: Number,
        roja: Number,
        caida: Number,
        postura: Number
    }
}, {
    timestamps: true
});
// alertSchema.virtual("patient_data", {
//     ref: "Patient",
//     localField: "patient",
//     foreignField: "_id",
//     justOne: true
// });

var autoPopulateLead = function(next) {
    this.populate('user');       
    next();
  };
alertSchema.pre('findOne', autoPopulateLead).pre('find', autoPopulateLead);

alertModel = mongoose.model('Alert', alertSchema);

module.exports = alertModel;