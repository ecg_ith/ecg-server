module.exports = {
    Alert: require('./alert'),
    AlertControl: require('./alert-control'),
    Device: require('./device'),
    User: require('./user')
};