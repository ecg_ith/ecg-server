const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const modelExists = mongoose.modelNames().includes('AlertControl');
let alertControlModel;

const alertControlSchema = new Schema({
    alert: { type: String, required: true, ref: 'Alert' },
    checkDate: { type: Date },
    sendDate: { type: Date, default: new Date() },  
    sendedBy: { type: String, ref: 'User' },      
    user: { type: String, ref: 'User' },
    checkedBy: { type: String, ref: 'User' }        
},
{
    timestamps: true
})

alertControlModel = mongoose.model('AlertControl', alertControlSchema);


module.exports = alertControlModel;
