const { isEmpty } = require('lodash');
const { Alert } = require('../models');
const { ClientError, ServerError } = require('../errors');
const DeviceService = require('./device');

function getById(id) {
    return Alert.findById(id)
        .then(alert => {
            return alert;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get alert ${id}: ${err.message}`);
        });
}

function getAll() {
    return Alert.find().populate("user")
        .then(alerts => {

            return alerts;
        })
        .catch(err => {
            throw new ServerError(`Error trying to get all alerts: ${err.message}`);
        });
}

function save(alert) {
    // Get device with alert.device (which is a UUID)
    return DeviceService.getById(alert.device)
    .then(device => {
        // Set alert.user to device.user
        alert.user = device.user

        // Save alert
        return Alert.create(alert)
        .catch(err => {
            throw new ServerError(`Error trying to save a new alert: ${err.message}`);
        })
    })
    .catch(err => {
        throw new ServerError(`Error trying to retrieve device with UUID ${alert.device}: ${err.message}`)
    })
}

function getUserAlerts(userId) {
    return Alert.find({ user: userId })
        .then(alerts => {
            return alerts;
        })
        .catch(err => {
            throw new ServerError(`Error trying to get all alerts: ${err.message}`);
        });
}

module.exports = {
    getById,
    getAll,
    save,
    getUserAlerts
}
