const { User } = require('../models');
const { ServerError, ClientError } = require('../errors');
const { isEmpty } = require('lodash');

function getAll() {
    return User.find()
        .then(users => {
            return users;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all users:
                ${err.message}`);
        });
}

function findAllByRole(role) {
    return User.find({ role })
        .then(users => {
            return users;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all users:
                ${err.message}`);
        });
}

function getById(uuid) {
    return User.findById(uuid)
        .then(user => {
            if (isEmpty(user)) {
                throw new ClientError(404, `User with ${uuid} uuid was not
                    found`);
            }
            return user;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get user by UUID:
                ${err.message}`);
        })
}

function getUserByLinkedUsers(userId) {
    return User.find({ linkedUsers: userId })
        .then(users => {
            return users;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all users:
                ${err.message}`);
        });
}

function save(user) {
    return User.create(user)
        .catch(err => {
            throw new ServerError(`Error trying to save a new user:
                ${err.message}`);
        });
}

function deleteUser(uuid) {
    return User.findByIdAndRemove(uuid)
        .catch(err => {
            throw new ServerError(`Error trying to delete user ${uuid}:
                ${err.message}`);
        });
}

function update(userId, user) {
    return User.update(
        { _id: userId },
        user
    )
    .then(() => getById(userId))
    .catch(err => {
        throw new ServerError(`Error trying to update user ${userId}:
            ${err.message}`);
    })
}

function authenticate(email, password) {
    return new Promise(function(resolve, reject) {
        User.authenticate(email, password, function(error, user) {
            if (error || !user) {
                var err = new Error('Wrong email or password.');
                err.status = 401;
                reject(err);
            } else {
                resolve(user);
            }
        });
    });
    
}

function getUsersByUserIds(users) {
    return User.find({ _id: {$in: users}})
    .then(users => {
        return users;
    })
    .catch(err => {
        if (err instanceof ClientError) {
            throw err;
        }

        throw new ServerError(`Error trying to get all users:
            ${err.message}`);
    });
}

module.exports = {
    getAll,
    getById,
    save,
    delete: deleteUser,
    update,
    authenticate,
    getUsersByUserIds,
    getUserByLinkedUsers
};
