const { Device } = require('../models');
const { ServerError, ClientError } = require('../errors');
const { isEmpty } = require('lodash');

function getAll() {
    return Device.find()
        .then(devices => {
            if (isEmpty(devices)) {
                throw new ClientError(404, `No devices found`);
            }

            return devices;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all devices:
                ${err.message}`);
        });
}

function getById(uuid) {
    return Device.findById(uuid)
        .then(device => {
            if (isEmpty(device)) {
                throw new ClientError(404, `Device with ${uuid} uuid was not
                    found`);
            }
            return device;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get device by UUID:
                ${err.message}`);
        })
}

function save(device) {
    return Device.create(device)
        .catch(err => {
            throw new ServerError(`Error trying to save a new device:
                ${err.message}`);
        });
}

function deleteDevice(uuid) {
    return Device.findByIdAndRemove(uuid)
        .catch(err => {
            throw new ServerError(`Error trying to delete device ${uuid}:
                ${err.message}`);
        });
}

function update(deviceId, device) {
    return Device.update(
        { _id: deviceId },
        device
    )
    .then(() => getById(deviceId))
    .catch(err => {
        throw new ServerError(`Error trying to update device ${deviceId}:
            ${err.message}`);
    })
}

module.exports = {
    getAll,
    getById,
    save,
    delete: deleteDevice,
    update
};
