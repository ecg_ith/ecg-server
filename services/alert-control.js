const { AlertControl } = require('../models');
const AlertService = require('./alert');
const { ServerError, ClientError } = require('../errors');
const { isEmpty } = require('lodash');
const userService = require('./user');
const { doctors } = require('../sockets');

function getAll() {
    return AlertControl.find({ user: '-1', checkedBy:  { $exists: false }  }).populate("alert")
        .then(alertControls => {
            return alertControls;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all alerts:
                ${err.message}`);
        });
}
function getAlertControlHistory() {
    return AlertControl.find().populate("user").populate("checkedBy").populate("sendedBy")
        .then(alertControls => {
            return alertControls;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all alerts:
                ${err.message}`);
        });
}

function getById(id) {
    return AlertControl.findById(id)
        .then(alertControl => {
            if (isEmpty(alertControl)) {
                throw new ClientError(404, `AlertControl with ${uuid} id was not
                    found`);
            }
            return alertControl;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get alertControl by ID:
                ${err.message}`);
        })
}

function save(alertControl) {
    return AlertControl.create(alertControl)
        .catch(err => {
            throw new ServerError(`Error trying to save a new alertControl:
                ${err.message}`);
        });
}

function update(alertControlId, alertControl) {
    return AlertControl.update(
        { _id: alertControlId },
        alertControl
    )
    .then(() => getById(alertControlId))
    .catch(err => {
        throw new ServerError(`Error trying to update alertControl ${alertControlId}:
            ${err.message}`);
    })
}

function checkAlertControl(alertControlId, userId) {
    return getById(alertControlId)
    .then((alertControl) => {
        alertControl.checkedBy = userId;
        alertControl.checkDate = new Date();
        return AlertControl.update({ _id: alertControlId }, alertControl);
    })
    .catch(err => {
        throw new ServerError(`Error trying to update alertControl ${alertControlId}:
            ${err.message}`);
    })
}

function sendAlertToDoctor(alertControlId, alertId, userId, patientId) {
    if(alertControlId) {
        checkAlertControl(alertControlId, userId);
    }
    return userService.getUserByLinkedUsers(patientId).then(users => {
        if(users.length > 0) {
            users.forEach(user => {
                const alertControl = {
                    alert: alertId,
                    user: user._id,
                    sendedBy: userId
                }
                save(alertControl).then(alertControl => {
                    if(doctors[user._id]) {
                        userService.getById(patientId).then(patient => {
                            AlertService.getById(alertId).then( alert => {
                                doctors[user._id].send(JSON.stringify({alertControl, alert, patient}));
                            })
                        })
                    }
                });
                
            })
        }
    })
    
}

function getAlertControlsByUserId(userId) {
    return AlertControl.find({ user: userId, checkedBy:  { $exists: false }  }).populate("alert")
        .then(alertControls => {
            return alertControls;
        })
        .catch(err => {
            if (err instanceof ClientError) {
                throw err;
            }

            throw new ServerError(`Error trying to get all alerts:
                ${err.message}`);
        });
}

module.exports = {
    getAll,
    getById,
    save,
    update,
    checkAlertControl,
    sendAlertToDoctor,
    getAlertControlsByUserId,
    getAlertControlHistory
};
