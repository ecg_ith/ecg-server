module.exports = {
    AlertService: require('./alert'),
    AlertControlService: require('./alert-control'),
    DeviceService: require('./device'),
    UserService: require('./user')    
};
