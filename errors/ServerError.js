module.exports = class ServerError extends Error {
    constructor(message) {
        super(message);
        Error.captureStackTrace(this, ServerError);
        this.statusCode = 500;
        this.message = message;
    }
}