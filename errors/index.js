module.exports = {
    ClientError: require('./ClientError'),
    ServerError: require('./ServerError')
};