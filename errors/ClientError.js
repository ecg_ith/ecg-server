module.exports = class ClientError extends Error {
    constructor(statusCode, message) {
        super(message);
        Error.captureStackTrace(this, ClientError);
        this.statusCode = statusCode;
        this.message = message;
    }
}