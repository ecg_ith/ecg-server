const { isEmpty } = require('lodash');
const { ClientError } = require('../errors');

function alertControlId(req, res, next) {
    const { alertControlId } = req.params;

    if (isEmpty(alertControlId)) {
        return next(new ClientError(400, 'Missing alertControlId parameter'));
    }

    res.locals.alertControlId = alertControlId;
    next();
}

function deviceId(req, res, next) {
    const { deviceId } = req.params;

    if (isEmpty(deviceId)) {
        return next(new ClientError(400, 'Missing deviceId parameter'));
    }

    res.locals.deviceId = deviceId;
    next();
}

function userId(req, res, next) {
    const { userId } = req.params;

    if (isEmpty(userId)) {
        return next(new ClientError(400, 'Missing userId parameter'));
    }

    res.locals.userId = userId;
    next();
}

function role(req, res, next) {
    const { role } = req.params;

    if (isEmpty(role)) {
        return next(new ClientError(400, 'Missing userId parameter'));
    }

    res.locals.role = role;
    next();
}

function alertId(req, res, next) {
    const { alertId } = req.params;

    if (isEmpty(alertId)) {
        return next(new ClientError(400, 'Missing alertId parameter'));
    }

    res.locals.alertId = alertId;
    next();
}

module.exports = {
    userId,
    deviceId,
    alertId,
    alertControlId,
    role
}
