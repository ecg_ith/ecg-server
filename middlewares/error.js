const { ClientError, ServerError } = require('../errors');

module.exports = (err, req, res, next) => {
    let status = 500;

    if (err instanceof ClientError || err instanceof ServerError) {
        status = err.statusCode
    }

    console.error(err);

    res.status(status)
        .send(err.message);
};
