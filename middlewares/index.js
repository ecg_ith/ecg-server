module.exports = {
    headers: require('./headers'),
    error: require('./error'),
    params: require('./params')
};
