const { DeviceService } = require('../services');

function getDevices(req, res, next) {
    DeviceService.getAll()
        .then(devices => res.json(devices))
        .catch(err => next(err));
}

function getDevice(req, res, next) {
    const { deviceId } = res.locals;

    DeviceService.getById(deviceId)
        .then(device => res.json(device))
        .catch(err => next(err));
}

function createDevice(req, res, next) {
    const device = req.body;

    DeviceService.save(device)
        .then(device => res.json(device))
        .catch(err => next(err));
}

function deleteDevice(req, res, next) {
    const { deviceId } = res.locals;

    DeviceService.delete(deviceId)
        .then(() => res.sendStatus(200))
        .catch(err => next(err));
}

function updateDevice(req, res, next) {
    const { deviceId } = res.locals;

    DeviceService.update(deviceId, device)
        .then(device => res.json(device))
        .catch(err => next(err));
}

module.exports = {
    getDevices,
    getDevice,
    createDevice,
    deleteDevice,
    updateDevice
};
