const { AlertService } = require('../services');

function getAlerts (req, res, next) {
    AlertService.getAll()
        .then(alerts => res.json(alerts))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const { alertId } = req.params;

    AlertService.getById(alertId)
        .then(alert => res.json(alert))
        .catch(err => next(err));
}

function getUserAlerts(req, res, next) {
    const { userId } = res.locals;

    AlertService.getUserAlerts(userId)
        .then(alerts => res.json(alerts))
        .catch(err => next(err));
}


module.exports = {
    getAlerts,
    getById,
    getUserAlerts
};
