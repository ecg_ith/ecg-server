const { AlertControlService } = require('../services');

function getAlertControls(req, res, next) {
    AlertControlService.getAll()
        .then(alertControls => res.json(alertControls))
        .catch(err => next(err));
}

function getAlertControlHistory(req, res, next) {
    AlertControlService.getAlertControlHistory()
        .then(alertControls => res.json(alertControls))
        .catch(err => next(err));
}

function getAlertControl(req, res, next) {
    const { alertControlId } = res.locals;

    AlertControlService.getById(alertControlId)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

function createAlertControl(req, res, next) {
    const alertControl = req.body;

    AlertControlService.save(alertControl)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

function updateAlertControl(req, res, next) {
    const { alertControlId } = res.locals;
    const alertControl = req.body;

    AlertControlService.update(alertControlId, alertControl)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

function checkAlertControl(req, res, next) {
    const { alertControlId, userId } = req.body;

    AlertControlService.checkAlertControl(alertControlId, userId)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

function sendAlertToDoctor(req, res, next) {
    const { alertControlId, alertId, userId, patientId } = req.body;

    AlertControlService.sendAlertToDoctor(alertControlId, alertId, userId, patientId)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

function getAlertControlsByUserId(req, res, next) {
    const { userId } = res.locals;

    AlertControlService.getAlertControlsByUserId(userId)
        .then(alertControl => res.json(alertControl))
        .catch(err => next(err));
}

module.exports = {
    getAlertControls,
    getAlertControl,
    createAlertControl,
    updateAlertControl,
    checkAlertControl,
    sendAlertToDoctor,
    getAlertControlsByUserId,
    getAlertControlHistory
};
