const { UserService } = require('../services');

function getUsers(req, res, next) {
    UserService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function findAllByRole(req, res, next) {
    const { role } = res.locals;
    
    UserService.findAllByRole(role)
        .then(users => res.json(users))
        .catch(err => next(err));
}


function getUser(req, res, next) {
    const { userId } = res.locals;

    UserService.getById(userId)
        .then(user => res.json(user))
        .catch(err => next(err));
}

function createUser(req, res, next) {
    const user = req.body;

    UserService.save(user)
        .then(user => res.json(user))
        .catch(err => next(err));
}

function deleteUser(req, res, next) {
    const { userId } = res.locals;

    UserService.delete(userId)
        .then(() => res.sendStatus(200))
        .catch(err => next(err));
}

function updateUser(req, res, next) {
    const { userId } = res.locals;
    const user = req.body;
    
    UserService.update(userId, user)
        .then(user => res.json(user))
        .catch(err => next(err));
}

function authenticate(req, res, next) {
    const { email, password } = req.body;

    UserService.authenticate(email, password)
        .then(user =>{
            req.session = {}
            req.session.userId = user._id;
            res.json(user);
        })
        .catch(err => next(err));
}
function getUsersByUserIds(req, res, next) {
    const { users } = req.body;
    
    UserService.getUsersByUserIds(users)
        .then(users => res.json(users))
        .catch(err => next(err));
}

module.exports = {
    getUsers,
    findAllByRole,    
    getUser,
    createUser,
    deleteUser,
    updateUser,
    authenticate,
    getUsersByUserIds
};
