let { DeviceService, UserService, AlertService, AlertControlService } = require('../services')
setTimeout(()=> {
    AlertControlService = require('../services').AlertControlService
    DeviceService = require('../services').DeviceService
    UserService = require('../services').UserService
    AlertService = require('../services').AlertService
}, 1000)

function saveAlert(alert) {
    return AlertService.save(alert);
}

function getDevice(uuid) {
    return DeviceService.getById(uuid);
}

function getUserByLinkedUsers(userId) {
    return UserService.getUserByLinkedUsers(userId);
}

function saveAlertControl(alertControl) {
    return AlertControlService.save(alertControl);
}

function getPatient(id) {
    return UserService.getById(id)
}


module.exports = {
    saveAlert,
    getDevice,
    getUserByLinkedUsers,
    saveAlertControl,
    getPatient
}