const { AlertService } = require('../services');
const { forEach, filter } = require('lodash');
const mango = require('./mango');

function connection(ws) {
	console.log("Connection established");
	ws.on('message', function incoming(data) {
		data = data.toString()
		// AUTH:2be7febd-6e7f-4d58-b34d-282d39a2b246
		if(data.startsWith('AUTH:') && data.length == 41) {
			let uuid = data.split(':')[1];
			ws.uuid = uuid
			devices[uuid] = ws;
			console.log('Autenticate: ' + uuid)
		}
		// AUTH:XEX0101010000
		if(data.startsWith('AUTH:') && data.length < 18) {
			let rfc = data.split(':')[1];
			ws.rfc = rfc
			doctors[rfc] = ws
			console.log('Autenticate: ' + rfc)
		}
		// REQUEST:ECG:2be7febd-6e7f-4d58-b34d-282d39a2b246 && data.length == 48
		if(data.startsWith('REQUEST:ECG:')) {
			let uuid = data.split(':')[2];
			if(devices[uuid]) {
				devices[uuid].send('REQUEST:ECG');
				console.log("REQUEST:ECG to " + uuid);
			}else {
				console.error('No se encontro ' + uuid);
			}
		}
		// RESPONSE:ECG:{"ecg":[...],"caida":0,"postura":1,...}
		if(data.startsWith('RESPONSE:ECG:')) {
			let payload = data.split('RESPONSE:ECG:')[1];
			let rawAlert;
			try {
				rawAlert = JSON.parse(payload);

				let alertRecived = {
					details: rawAlert,
					device: ws.uuid,
					diagnosis: "",
					observations: ""
				}
				//alert control evryone 
				//and create the alert 
				console.log(AlertService)
				mango.saveAlert(alertRecived)
					.then(alert => Promise.all([alert, mango.getDevice(ws.uuid)]))
					.then(([alert, device]) => Promise.all([alert, mango.getPatient(device.user)]))
					.then(([alert, patient]) => {
						var alertControl = {
							alert: alert._id,
							user: '-1',
							sendedBy: '-1'
						}
						mango.saveAlertControl(alertControl);
						// mango.getUserByLinkedUsers(patient._id).then(doctors => {+
						// 	doctors.forEach((doctor) => {
						// 		alertControl.user = doctor._id;
						// 		mango.saveAlertControl(alertControl);
						// 	})
						// })
						// filter(patient.doctors, doctorRfc => doctorRfc in doctors)
						// 	.forEach(doctorRfc => doctors[doctorRfc].send(alert));
						doctors.forEach(doctor => {
							doctor.send(JSON.stringify({alertControl, alert, patient}));
						})
						// doctors['pp1'].send(JSON.stringify({alertControl, alert, patient}));
					})
					.catch(err => {
						console.error(`Error responding to alert: ${err.message}`);
					});

			} catch(err) {
				console.log(`Time: ${new Date().toLocaleTimeString()}: Length: ${data.length}`);
				console.error(err);
			}
		}
	});
	ws.on('error', () => console.log('errored'));
}

var devices = {}
var doctors = {}

module.exports = {
	connection,
	devices,
	doctors
};