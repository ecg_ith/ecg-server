const express = require('express');
const http = require('http');
const url = require('url');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const routes = require('./routes');
// const { MONGO_URI } = require('./config/environment');
const WebSocket = require('ws');
const { headers, error } = require('./middlewares');
const Promise = require('bluebird');
var session = require('express-session')

mongoose.Promise = Promise;

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(headers);
app.use('/api', routes);
app.use(error);

//use sessions for tracking logins
app.use(session({
  secret: 'SuperSecretShh',
  resave: true,
  saveUninitialized: false
}));

const server = http.createServer(app);
const wss = new WebSocket.Server({ server,
  verifyClient: function(info) {
    // console.log(info);
    return true;
  }
});
wss.on('connection', require('./sockets').connection);

setInterval(() => {
  wss.clients.forEach((client) => {
    client.send(new Date().toTimeString());
  });
}, 1000);

var alertControl = require('./services/alert-control')

// alertControl.save({ 
//   alert: '5a8735951c2ff7025ebe838e',       
//   user: -1});

mongoose.connect('mongodb://ecg:ecg2018@ds223609.mlab.com:23609/arrhythmia-detection', 
{ useMongoClient: true })
  .then(() => {
    server.listen(process.env.PORT || 8080, function listening() {
      console.log('Listening on %d', server.address().port);
    });
  });
