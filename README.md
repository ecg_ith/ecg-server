# ECG Server
## Setup Instructions
Install the project's dependencies running
```bash
$ npm install
```

Install MongoDB Community Edition with Homebrew

Update Homebrew’s package database.

```bash
$ brew update
```

Install MongoDB.

```bash
$ brew install mongodb --with-openssl
```
Create the data directory.

```bash
$ mkdir -p /data/db
```

Run MongoDB. If necessary, specify the path of the mongod or the data directory.

```bash
$ mongod
```

Connect to database using mongoose.

Create a .env file at the root of ecg-server repository. Inside the file, create the following enviroment variables 

```bash
NODE_ENV=development
MONGO_URI=mongodb://localhost:27017/arrhythmia-detection
```


Finally you can run the project with:

```bash
npm start
```

