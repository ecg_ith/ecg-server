require('dotenv').config();

const env = {
    NODE_ENV: process.env.NODE_ENV,
    MONGO_URI: process.env.MONGO_URI
};

for (let key in env) {
    if (env[key] === undefined) {
        // throw new Error(`Expected ${key} environment variable is undefined`);
    }
}

module.exports = env;
